import urllib2
import re
import os
import json

def remove_html_tags(html):

	return re.sub('<[^<]+?>', '', html)

def get_cards_by_type(card_type, download_images, card_images_folder):

	page_name    = ''

	if card_type == 'Spell':
		page_name = 'ability'
	elif card_type == 'Minion':
		page_name = 'minion'
	elif card_type == 'Weapon':
		page_name = 'weapon'
	else:
		return []

	image_file_ext = 'png'
	landing_page = 'http://www.hearthpwn.com/cards/' + page_name
	
	spell_response = urllib2.urlopen(landing_page)
	spell_response_html = spell_response.read()

	pages = 1
	
	cards = []
	
	pages_div = re.search(r'<div class="b-pagination b-pagination-a">(.*?)</div>', spell_response_html, re.DOTALL | re.MULTILINE)
	
	for page in re.finditer(r'<a href="/cards/' + page_name + '?(.*?&)*?page=(?P<page>\d+)(.*?&)*?" class="b-pagination-item">(?P=page)</a>', pages_div.group(1), re.DOTALL | re.MULTILINE):
		pages = int(page.group('page'))
	
	for i in range(0, pages):
	
		if not i == 0:
		
			spell_response = urllib2.urlopen(landing_page + '?page=' + str(i + 1))
			spell_response_html = spell_response.read()
			
		for card_match in re.finditer(r'<img class="hscard-static" src="(?P<image_src>[^"]+)".*?<td class="visual-details-cell">\s*<h3>(?P<name>.*?)</h3>\s*(<p>(?P<description>.*?)</p>)?\s*<ul>(?P<list>.*?)</ul>', spell_response_html, re.DOTALL | re.MULTILINE):
			
			card_info = {
				'name'        : remove_html_tags(card_match.group('name')).strip(),
				'id'          : int(card_match.group('image_src').split('/')[-1].split('.')[0])
			}
			
			if card_match.group('description'):
				card_info['description'] = remove_html_tags(card_match.group('description')).strip()
			
			for info in re.findall('<li>(.*?):(.*?)</li>', card_match.group('list'), re.DOTALL | re.MULTILINE):
				card_info[info[0].lower()] = remove_html_tags(info[1]).strip()
			
			card_info['type'] = card_type
			
			if (download_images):
				
				f = open(card_images_folder + os.sep + str(card_info['id']) + '.' + image_file_ext, 'wb')
				f.write(urllib2.urlopen(card_match.group('image_src')).read())
			
			cards.append(card_info)
		
	return cards
	
if __name__ == '__main__':

	images_folder   = './card_images'
	download_images = True
	
	spells  = get_cards_by_type('Spell',  download_images, images_folder)
	minions = get_cards_by_type('Minion', download_images, images_folder)
	weapons = get_cards_by_type('Weapon', download_images, images_folder)
	
	f = open('cards.json', 'w')
	f.write(json.dumps({'spells' : spells, 'minions' : minions, 'weapons' : weapons }))