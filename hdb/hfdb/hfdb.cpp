#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/program_options.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/copy.hpp>
//#include <boost/archive/iterators/ostream_iterator.hpp>

#include <sstream>
#include <set>
#include <vector>

#include <adhmatching/ADH.h>

#define HEARTHSTONE_CARD_TYPE_MINION (0)
#define HEARTHSTONE_CARD_TYPE_SPELL  (1)
#define HEARTHSTONE_CARD_TYPE_WEAPON (2)

int main(int argc, char *argv[])
{

	boost::program_options::options_description desc("Options");

	desc.add_options()
		("help", "Prints help message")
		("min", boost::program_options::value<int>(), "Sets the limit for the minimum keypoints for each image")
		("max", boost::program_options::value<int>(), "Sets the limit for the maximum keypoints for each image")
		("ignore", boost::program_options::value<std::vector<int>>(), "Sets the list of ids to ignore");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::command_line_parser(argc, argv).
		options(desc).run(), vm);
	boost::program_options::notify(vm);

	std::set<int> exceptions;

	if (vm.count("help"))
	{
		desc.print(std::cout);
		return 0;
	}

	if (vm.count("ignore"))
	{
		std::vector<int> ignore_list = vm["ignore"].as<std::vector<int>>();
		exceptions.insert(ignore_list.begin(), ignore_list.end());
	}

	int min_kp = 20;
	int max_kp = 30;

	if (vm.count("min"))
	{
		min_kp = vm["min"].as<int>();
	}

	if (vm.count("max"))
	{
		min_kp = vm["max"].as<int>();
	}

	std::string cards_images_folder = "./card_images";

	std::ifstream hdb("cards.json");

	boost::property_tree::ptree pt;
	boost::property_tree::read_json(hdb, pt);

	const char * card_types[3] = { "minions", "spells", "weapons" };

	cv::Mat masks[3] =
	{
		cv::imread("minion_mask.png", CV_LOAD_IMAGE_GRAYSCALE),
		cv::imread("spell_mask.png", CV_LOAD_IMAGE_GRAYSCALE),
		cv::imread("weapon_mask.png", CV_LOAD_IMAGE_GRAYSCALE)
	};

	std::vector<cv::Mat> descriptorsCollection;

	std::vector<int> cards_ids;
	std::vector<std::string> cards_names;
	std::vector<int> cards_types;

	int cards_count = 0;

	/* Minions begin */
	
	for (int card_type_i = 0; card_type_i < 3; card_type_i++)
	{

		for (auto it : pt.get_child(card_types[card_type_i]))
		{

			int id = it.second.get<int>("id");

			if (exceptions.find(id) == exceptions.end())
			{

				std::string name = it.second.get<std::string>("name");

				auto f = cards_images_folder + '/' + std::to_string(id) + ".png";
				cv::Mat img = cv::imread(f, CV_LOAD_IMAGE_GRAYSCALE);

				cv::equalizeHist(img, img);

				if (img.size().area() == 0)
				{
					std::cout << "No image for card " << id << " (" << name << ")" << std::endl;
				}
				else
				{

					int hessian = 10000;
					bool done = false;

					do 
					{

						cv::SurfFeatureDetector fd(hessian);

						std::vector<cv::KeyPoint> keypoints;
						cv::Mat descriptors;

						fd.detect(img, keypoints, masks[card_type_i]);
						fd.compute(img, keypoints, descriptors);

						if (descriptors.rows < min_kp && hessian > 599)
						{
							hessian -= 599;
						}
						else if (descriptors.rows > max_kp && hessian < 100000)
						{
							hessian += 593;
						}
						else
						{
							
							cards_count++;

							descriptorsCollection.push_back(descriptors);

							cards_ids.push_back(id);
							cards_names.push_back(name);
							cards_types.push_back(card_type_i);

							done = true;

						}

					} while (!done);

				}

			}

		}

	}

	adh::SerializableFlannBasedMatcher matcher;

	matcher.add(descriptorsCollection);
	matcher.train();

	matcher.writeIndex("index.bin");

	cv::FileStorage fdb("fdb.yml", cv::FileStorage::WRITE);

	fdb << "cards" << "{";
	fdb << "count" << (int)cards_ids.size();
	fdb << "ids" << cards_ids;
	fdb << "names" << cards_names;
	fdb << "types" << cards_types;
	fdb << "descriptors" << "[";
	for (auto &d : descriptorsCollection) fdb << d;
	fdb << "]";
	fdb << "}";

	fdb << "masks" << "[" << masks[0] << masks[1] << masks[2] << "]";

	fdb.release();

	return 0;

}

