#include <opencv2/opencv.hpp>

#include <ctime>
#include <fstream>

int main(int argc, char *argv[])
{

	srand(time(NULL));

	std::string card_images = "card_images";

	cv::FileStorage fdb("fdb.yml", cv::FileStorage::READ);

	std::vector<int> ids;

	fdb["cards"]["ids"] >> ids;

	fdb.release();

	cv::Mat bg = cv::imread("bg.png");

	int positions[3][2] = { { 371, 221 }, { 649, 221 }, { 930, 221 } };
	std::vector<int> cardIds;

	for (int i = 0; i < 3; i++)
	{

		cv::Mat card;
		int id;
		
		do 
		{
			id = rand() % ids.size();
			card = cv::imread(card_images + "\\" + std::to_string(id) + ".png", cv::IMREAD_UNCHANGED);
		} while (!card.rows);
		

		cv::resize(card, card, cv::Size(0, 0), 0.985f, 0.985f);

		cv::Mat bgra[4];
		cv::split(card, bgra);

		cv::Mat bgr;
		cv::merge(bgra, 3, bgr);

		bgr.copyTo(bg(cv::Rect(positions[i][0], positions[i][1], bgr.cols, bgr.rows)), bgra[3]);

		cardIds.push_back(id);

	}

	cv::imwrite("screen.png", bg);

	std::ofstream out("screen_cards.txt");
	out << cardIds[0] << " " << cardIds[1] << " " << cardIds[2];
	
	return 0;

}