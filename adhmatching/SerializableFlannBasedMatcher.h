#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>

namespace adh
{

	struct SerializableFlannBasedMatcher :
		public cv::FlannBasedMatcher
	{

		void printParams();

		void readIndex(const char *filename);
		void writeIndex(const char *filename);

	};


}