#include "SerializableFlannBasedMatcher.h"

#include <vector>

using namespace adh;

void SerializableFlannBasedMatcher::printParams()
{
	printf("SaveableMatcher::printParams: \n\t"
		"addedDescCount=%d\n\t"
		"flan distance_t=%d\n\t"
		"flan algorithm_t=%d\n",
		addedDescCount,
		flannIndex->getDistance(),
		flannIndex->getAlgorithm());

	std::vector<std::string> names;
	std::vector<int> types;
	std::vector<std::string> strValues;
	std::vector<double> numValues;

	indexParams->getAll(names, types, strValues, numValues);

	for (size_t i = 0; i < names.size(); i++)
		printf("\tindex param: %s:\t type=%d val=%s %.2f\n",
		names[i].c_str(), types[i],
		strValues[i].c_str(), numValues[i]);

	names.clear();
	types.clear();
	strValues.clear();
	numValues.clear();
	searchParams->getAll(names, types, strValues, numValues);

	for (size_t i = 0; i < names.size(); i++)
		printf("\tsearch param: %s:\t type=%d val=%s %.2f\n",
		names[i].c_str(), types[i],
		strValues[i].c_str(), numValues[i]);
}

void SerializableFlannBasedMatcher::readIndex(const char *filename)
{

	indexParams->setAlgorithm(cvflann::FLANN_INDEX_SAVED);
	indexParams->setString("filename", filename);

	train();

	printParams();

}

void SerializableFlannBasedMatcher::writeIndex(const char *filename)
{
	flannIndex->save(filename);
}