#include <Windows.h>

#include <sstream>
#include <fstream>
#include <cstdlib>

#include "Bitmap.h"
#include "HearthstoneExtractor.h"
#include "HearthstoneArenaMatcher.h"

#include <opencv2/opencv.hpp>

#define ADB_OK 0
#define ADH_DB_LOAD_ERROR 1


int CALLBACK WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{

	adh::HearthstoneArenaMatcher matcher;

	if (!matcher.Create())
	{
		MessageBox(NULL, "An error occurred while loading the database.", "Error", MB_OK | MB_ICONERROR);
		return ADH_DB_LOAD_ERROR;
	}

	cv::Mat screen = cv::imread("screen.png");
	cv::cvtColor(screen, screen, CV_BGR2GRAY);

	auto results = matcher.Match(screen);

	std::ofstream out("out.txt");
	std::ofstream hout("out-names.txt");

	for (int i = 0; i < 3; i++)
	{
		out << results.ids[i] << " ";
		hout << results.names[i] << " ";
	}

	return ADB_OK;

}