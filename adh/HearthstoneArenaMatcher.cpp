#include "HearthstoneArenaMatcher.h"

#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/features2d.hpp>

using namespace adh;

HearthstoneArenaMatcher::HearthstoneArenaMatcher(void)
{

}


HearthstoneArenaMatcher::~HearthstoneArenaMatcher(void)
{
	Destroy();
}

bool HearthstoneArenaMatcher::Create(void)
{

	bool success = true;

	try
	{
		cv::FileStorage fdb("fdb.yml", cv::FileStorage::READ);

		auto cards_node = fdb["cards"];

		cards_node["ids"]   >> m_ids;
		cards_node["names"] >> m_names;
		cards_node["types"] >> m_types;

		std::vector<cv::Mat> descriptorsCollection;

		for (auto node : cards_node["descriptors"])
		{
			cv::Mat descriptors;
			node >> descriptors;
			descriptorsCollection.push_back(descriptors);
		}

		fdb.release();

		m_matcher.add(descriptorsCollection);
		m_matcher.readIndex("index.bin");

	}
	catch (std::exception &)
	{
		success = false;
	}

	return success;

}

void HearthstoneArenaMatcher::Destroy(void)
{

	m_ids.clear();
	m_names.clear();
	m_types.clear();

	m_matcher.clear();

}

HearthstoneArenaMatcher::MathResult HearthstoneArenaMatcher::Match(cv::Mat screen)
{


	MathResult result;
	cv::SURF fd(100);

	int w     = screen.cols;
	int h     = screen.rows;
	int rw    = screen.rows * 4 / 3.0f;
	int rh    = 24 * h / 25.0f;

	int bw    = (w - rw) / 2;

	int roiw  = rw / 7.0f;
	int roih  = rh / 5.9f;
	int roix  = rw / 8.5f + bw;
	int roiy  = rh / 4.6f;

	int roidx = rw / 5.15f;

	for (int i = 0; i < 3; i++)
	{

		cv::Rect r(roidx * i + roix, roiy, roiw, roiy);

		cv::Mat cardImage(screen(r));

		cv::equalizeHist(cardImage, cardImage);

		std::vector<cv::KeyPoint> screen_keypoints;
		cv::Mat screen_descriptors;

		fd.detect(cardImage, screen_keypoints);
		fd.compute(cardImage, screen_keypoints, screen_descriptors);

		std::vector<cv::DMatch> matches;

		int max_matches = 0;

		m_matcher.match(screen_descriptors, matches);
		
		int best_match_id = -1;
		int best_match_count = 0;
		
		std::vector<int> good_matches(m_ids.size());

		for (auto &match : matches)
		{
			if (match.distance < 0.35f)
			{
				good_matches[match.imgIdx]++;
			}
		}

		auto it = std::max_element(good_matches.begin(), good_matches.end());

		if (*it)
		{

			int imgIndex = it - good_matches.begin();

			result.ids[i]   = m_ids[imgIndex];
			result.names[i] = m_names[imgIndex];
			result.types[i] = m_types[imgIndex];

		}
		else
		{
			result.ids[i] = -1;
		}


		/*for (HearthstoneDB::Iterator it = m_hdb->begin(); it != m_hdb->end(); it++)
		{

			matcher.match(screen_descriptors, (*it).descriptors, matches);

			int good_matches = 0;

			for (auto &match : matches)
			{

				if (match.distance < 0.35f)
				{
					good_matches++;
				}

			}

			if (good_matches > max_matches)
			{
				max_matches = good_matches;
				result.cards[i] = it;
			}

		}*/

	}

	return result;

}