#pragma once

#include <opencv2/opencv.hpp>

#include <adhmatching/ADH.h>

namespace adh
{

	class HearthstoneArenaMatcher
	{

	public:

		struct MathResult
		{
			int         ids[3];
			std::string names[3];
			int         types[3];
		};

		HearthstoneArenaMatcher(void);
		~HearthstoneArenaMatcher(void);

		bool Create(void);
		void Destroy(void);

		MathResult Match(cv::Mat screen);

	private:

		adh::SerializableFlannBasedMatcher m_matcher;

		std::vector<int>         m_ids;
		std::vector<std::string> m_names;
		std::vector<int>         m_types;
		

	};

}
