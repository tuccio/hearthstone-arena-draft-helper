#include "HearthstoneExtractor.h"

#include <Wingdi.h>

#include "Bitmap.h"

#define HEARTHSTONE_WINDOW_TITLE (TEXT("Hearthstone"))

using namespace adh;

HearthstoneExtractor::HearthstoneExtractor(void) :
	m_hWnd(NULL)
{
}


HearthstoneExtractor::~HearthstoneExtractor(void)
{
	Close();
}

bool HearthstoneExtractor::Open(void)
{

	Close();

	bool result = false;
	
	HWND hWnd = FindWindow(nullptr, HEARTHSTONE_WINDOW_TITLE);

	if (hWnd)
	{
		m_hWnd = hWnd;
		result = true;
	}

	return result;

}

void HearthstoneExtractor::Close(void)
{
	m_hWnd = NULL;
}

cv::Mat HearthstoneExtractor::GetScreenshot(void)
{

	cv::Mat image;
	m_lastError = HSE_OK;

	if (!m_hWnd)
	{
		m_lastError = HSE_NOT_OPEN;
	}
	else
	{

		ShowWindow(m_hWnd, SW_SHOW);

		HDC hDC = GetWindowDC(m_hWnd);

		if (!hDC)
		{
			Close();
			m_lastError = HSE_TERMINATED;
		}
		else
		{
			
			HDC  hMemoryDC = CreateCompatibleDC(hDC);

			RECT clientRect;
			GetClientRect(m_hWnd, &clientRect);

			//int width  = clientRect.right;
			//int height = clientRect.bottom;

			DWORD dwStyle   = (DWORD) GetWindowLong(m_hWnd, GWL_STYLE);
			DWORD dwExStyle = (DWORD) GetWindowLong(m_hWnd, GWL_EXSTYLE);

			int width  = clientRect.right;
			int height = clientRect.bottom;

			AdjustWindowRectEx(&clientRect, dwStyle, FALSE, dwExStyle);

			HBITMAP hBitmap = CreateCompatibleBitmap(
				hDC,
				width,
				height);

			HBITMAP hOldBitmap = (HBITMAP) SelectObject(hMemoryDC, hBitmap);

			BitBlt(
				hMemoryDC,
				clientRect.left,
				clientRect.top,
				clientRect.right - clientRect.left,
				clientRect.bottom - clientRect.top,
				hDC, 0, 0, SRCCOPY);

			SelectObject(hMemoryDC, hOldBitmap);

			ReleaseDC(m_hWnd, hDC);

			BITMAPINFO bmi = GetBitmapInfo(hBitmap);

			image.create(bmi.bmiHeader.biHeight, bmi.bmiHeader.biWidth, CV_8UC4);
			GetDIBits(hMemoryDC, hBitmap, 0, bmi.bmiHeader.biHeight, image.data, &bmi, 0);

			cv::flip(image, image, 0);

			DeleteDC(hMemoryDC);

		}

	}

	return image;

}