#include "Bitmap.h"

BITMAPINFO adh::GetBitmapInfo(HBITMAP hBitmap)
{

	BITMAP bitmap;
	BITMAPINFO bmi;

	GetObject(hBitmap, sizeof(BITMAP), &bitmap);

	bmi.bmiHeader.biSize     = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth    = bitmap.bmWidth;
	bmi.bmiHeader.biHeight   = bitmap.bmHeight;
	bmi.bmiHeader.biPlanes   = bitmap.bmPlanes; // we are assuming that there is only one plane
	bmi.bmiHeader.biBitCount = bitmap.bmBitsPixel;

	// no compression this is an rgb bitmap
	bmi.bmiHeader.biCompression = BI_RGB;

	// calculate size and align to a DWORD (8bit), we are assuming there is only one plane.
	bmi.bmiHeader.biSizeImage = ((bmi.bmiHeader.biWidth * bitmap.bmBitsPixel + 31) & -31) * bmi.bmiHeader.biHeight;

	// all device colours are important
	bmi.bmiHeader.biClrImportant = 0;

	return bmi;

}