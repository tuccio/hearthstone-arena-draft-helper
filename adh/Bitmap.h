#pragma once

#include <Windows.h>

namespace adh
{

	BITMAPINFO GetBitmapInfo(HBITMAP hBitmap);

}
