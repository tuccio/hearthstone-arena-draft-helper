#pragma once

#include <Windows.h>

#include <opencv2/opencv.hpp>

namespace adh
{

	enum HearthstoneExtractorError
	{
		HSE_OK,
		HSE_NOT_OPEN,
		HSE_TERMINATED,
		HSE_FAILED
	};

	class HearthstoneExtractor
	{

	public:

		HearthstoneExtractor(void);
		~HearthstoneExtractor(void);

		bool Open(void);
		void Close(void);

		cv::Mat GetScreenshot(void);

		HearthstoneExtractorError GetLastError(void) const;

	private:

		HWND m_hWnd;
		HearthstoneExtractorError m_lastError;

	};

}

